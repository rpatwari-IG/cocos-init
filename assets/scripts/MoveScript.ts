const { ccclass, property } = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {



    // onLoad () {}

    private moveLeft: boolean = false;
    private moveRight: boolean = false;
    public speed: number = 500;

    start() {
        this.moveRight = true;
    }

    update(dt: number) {
        if (this.node.position.x >= this.node.parent.width / 2 - this.node.width / 2) {
            this.moveLeft = true;
            this.moveRight = false;
        }
        else if (this.node.position.x < -(this.node.parent.width / 2 - this.node.width / 2)) {
            this.moveLeft = false;
            this.moveRight = true;
        }

        if (this.moveLeft) {
            this.node.setPosition(this.node.x - this.speed * dt, this.node.y, this.node.z);
        }
        else if (this.moveRight) {
            this.node.setPosition(this.node.x + this.speed * dt, this.node.y, this.node.z);
        }
    }
}
